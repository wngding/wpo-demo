const http = require('http'),
      fs   = require('fs'),
      url  = require('url');

http.createServer((req, res) => {
  console.log(req.method, req.url);

  const path = url.parse(req.url).pathname;
  if(path === '/') {
    let data = fs.readFileSync('./index.html');
    res.end(data);
  } else {
    res.statusCode = 404;
    res.end('not found!');
  }
}).listen(8080);
