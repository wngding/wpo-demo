# wpo-demo

前端性能优化演示案例。

## 安装

```bash
git clone https://wngding@bitbucket.org/wngding/wpo-demo.git
cd wpo-demo
npm install
```

## 运行

```bash
npx http-server wd-01
```
