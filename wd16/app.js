let $ul = document.querySelector('ul');

function genDom1(num) { // 操作单个节点
  console.time('M1');
  for(let i=0; i<num; i++) {
    let $li = document.createElement('li');
    $li.textContent = i;
    $ul.appendChild($li);
  }
  console.timeEnd('M1');
}

function genDom2(num) { // 批量操作
  console.time('M2');
  let domLi = '';
  for(let i=0; i<num; i++) {
    domLi += `<li>${i}</li>`;
  }
  $ul.innerHTML = domLi;
  console.timeEnd('M2');
}

//genDom2(1000);

function changeDom1() {
  console.time('M1');
  const $listItems = $ul.querySelectorAll('li'),
        num = $listItems.length;
  for(let i=0; i<num; i++) {
    $listItems[i].innerText = num-i;
  }
  console.timeEnd('M1');
}

function changeDom2(num) {
  console.time('M2');
  let dom = '';
  for(let i=0; i<num; i++) {
    dom += `<li>${num-i}</li>`;
  }
  $ul.innerHTML = dom;
  console.timeEnd('M2');
}
