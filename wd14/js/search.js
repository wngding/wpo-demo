$(() => {
  const $fmSearch = $('form[name="formsearch"]');

  $fmSearch.submit((e)=>{
    e.preventDefault();
    
    const kw = $fmSearch.find('input[name="q"]').val(),
          addr = `https://cn.bing.com/search?q=${kw}+site:shuomeitech.com`,
          $baidu = $(`<a href=${addr} target="_blank"></a>`);

    if(kw === '') return;

    $baidu[0].click();
  });
});
